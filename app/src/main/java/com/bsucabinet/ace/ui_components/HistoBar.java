package com.bsucabinet.ace.ui_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Dimension;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.bsucabinet.ace.activities.R;

import java.awt.font.TextAttribute;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


public class HistoBar extends View {

    private int barColor;
    private int textColor;
    private boolean showText;
    private int textSize;


    private Map<Integer, Integer> marks;

    private Paint textPaint, barPaint, percentPaint, percentSmallPaint;
    private DisplayMetrics dMetrics;
    private int h, w, barHeight, paddingTop, paddingLeft;
    private DecimalFormat formatter;

    public HistoBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setVerticalScrollBarEnabled(true);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.HistoBar,
                0, 0);

        marks = new HashMap<>();

        try {
            barColor = a.getColor(R.styleable.HistoBar_barColor, Color.RED);
            textColor = a.getColor(R.styleable.HistoBar_textColor, Color.BLACK);
            showText = a.getBoolean(R.styleable.HistoBar_showText, true);
            textSize = a.getDimensionPixelSize(R.styleable.HistoBar_textSize, 0);
        } finally {
            a.recycle();
        }

        init();
    }

    private void init() {
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(textColor);

        if (textSize != 0) {
            textPaint.setTextSize(textSize);
        }

        barPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        barPaint.setStyle(Paint.Style.FILL);
        barPaint.setColor(barColor);

        percentPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        percentPaint.setColor(Color.WHITE);
        percentPaint.setTextSize(textSize);

        percentSmallPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        percentSmallPaint.setColor(Color.GRAY);
        percentSmallPaint.setTextSize(textSize);

        WindowManager wm = (WindowManager) this.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        dMetrics = new DisplayMetrics();
        display.getMetrics(dMetrics);

        formatter = new DecimalFormat("#.#");

        h = dMetrics.heightPixels;
        w = dMetrics.widthPixels;

        barHeight = h / 20;

        paddingTop = h / 24;
        paddingLeft = w / 13;

        setMinimumHeight((barHeight + paddingTop)* 7);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!marks.isEmpty()) {
            int y = 0;

            int sumOfMarks = 0;

            int max = 0;

            for (Map.Entry<Integer, Integer> entry: marks.entrySet()) {
                sumOfMarks += entry.getValue();
                if (entry.getValue() > max) {
                    max = entry.getValue();
                }
            }

            float maxFreq = (float)max /sumOfMarks;
            int baseBarWidth = w * 4 / 5;
            baseBarWidth /= maxFreq;


            for (int i = 10; i >= 4; i--) {
                float coeff = 0;
                String percent = "0";
                if (marks.containsKey(i)) {
                    int markCount = marks.get(i);
                    coeff = ((float) markCount) / sumOfMarks;
                    percent = Integer.toString(markCount) + "/" + Integer.toString(sumOfMarks) +
                            " (" + formatter.format(coeff * 100) + "%)";
                }
                float barWidth = baseBarWidth * coeff;
                drawOneBar(canvas, barWidth, barHeight, paddingLeft, paddingTop, y,
                        Integer.toString(i), percent);
                y += barHeight + paddingTop;
            }

        }
    }

    private void drawOneBar(Canvas canvas, float barWidth, int barHeight,
                            int paddingLeft, int paddingTop, int y, String text, String percent) {
        int realY = y + paddingTop;
        float textY = realY + ((float)barHeight/2) - (textPaint.descent()+textPaint.ascent())/2;
        float textX = paddingLeft/2 - textPaint.measureText(text) /2;
        canvas.drawRect(paddingLeft, realY,paddingLeft + barWidth, realY+barHeight, barPaint);
        canvas.drawText(text, textX, textY, textPaint);
        float percentTextWidth = percentPaint.measureText(percent);
        if (barWidth > percentTextWidth) {
            float percentPosX = paddingLeft + barWidth / 2 - percentTextWidth / 2;
            canvas.drawText(percent, percentPosX, textY, percentPaint);
        }
        else {
            float percentPosX = paddingLeft + barWidth + paddingLeft;
            canvas.drawText(percent, percentPosX, textY, percentSmallPaint);
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = measureDim(widthMeasureSpec, dMetrics.widthPixels);
        int height = measureDim(heightMeasureSpec, dMetrics.heightPixels);
        //setMeasuredDimension(width, height);
        setMeasuredDimension(width,  (barHeight + paddingTop)* 7);
    }

    private int measureDim(int measureSpec, int size) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = size;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }



    public boolean isShowText() {
        return showText;
    }

    public void setShowText(boolean showText) {
        this.showText = showText;
        invalidate();
        requestLayout();
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        invalidate();
        requestLayout();
    }

    public int getBarColor() {
        return barColor;
    }

    public void setBarColor(int barColor) {
        this.barColor = barColor;
        invalidate();
        requestLayout();
    }
    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
        invalidate();
        requestLayout();
    }

    public Map<Integer, Integer> getMarks() {
        return marks;
    }

    public void setMarks(Map<Integer, Integer> marks) {
        this.marks = marks;
        invalidate();
        requestLayout();
    }

}
