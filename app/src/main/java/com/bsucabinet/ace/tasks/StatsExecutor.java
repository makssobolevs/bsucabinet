package com.bsucabinet.ace.tasks;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.bsucabinet.ace.database.SubjectsContact;
import com.bsucabinet.ace.interfaces.AsyncResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class StatsExecutor extends AsyncTask<Void, Void, Void> {

    private static final String SEMESTER_ID = "ctlStudProgress1$cmbSemester";
    private static final String SOME_PARAMETER_ID = "ctlStudProgress1_txtROWGUID";
    private static final String SOME_PARAMETER_FOR_POST = "ctlStudProgress1$txtROWGUID";
    private static final String TABLE_ID = "ctlStudProgress1_tblProgress";

    Map<String, String> paramsLogin;
    SQLiteDatabase database;
    AsyncResponse delegate;

    public StatsExecutor(AsyncResponse activity, SQLiteDatabase db, Map<String, String> params){
        super();
        database = db;
        paramsLogin = new HashMap<>(params);
        delegate = activity;
    }

    @Override
    public Void doInBackground(Void... params) {
        try {
            database.execSQL("DELETE FROM subjects");
            prepareParams();
            String page = makePostRequest();
            putSubjectsInDatabase(page);
        }
        catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    protected  void onPostExecute(Void result) {
        delegate.onTaskFinish();
    }

    public void putSubjectsInDatabase(String page) {
        Document doc = Jsoup.parse(page);

        Element table = doc.getElementById(TABLE_ID);
        Elements rows = table.select("tr");
        String session = null;

        for (int i = 0; i < rows.size(); i++) {
            Element row = rows.get(i);
            Elements cols = row.select("td");
            if (cols.size() == 1 && cols.get(0).text().contains("курс")) {
                session = cols.get(0).text();
            }
            else if (cols.size() == 10) {
                String subjname = cols.get(1).text();
                String mark1 = cols.get(9).text();
                String mark2 = cols.get(8).text();
                try {
                    if (isInteger(mark1)) {
                        int m = Integer.parseInt(mark1);
                        putSubjectEntry(subjname, m, session);
                    }
                    else if (isInteger(mark2)) {
                        int m = Integer.parseInt(mark2);
                        putSubjectEntry(subjname, m, session);
                    }
                }
                catch (NumberFormatException e) {  }
            }

        }

    }
    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }

    public void putSubjectEntry(String subj, int mark, String session) {
        ContentValues values = new ContentValues();
        values.put(SubjectsContact.Subjects.COLUMN_NAME_SUBJECT_TITLE, subj);
        values.put(SubjectsContact.Subjects.COLUMN_NAME_MARK, mark);
        values.put(SubjectsContact.Subjects.COLUMN_NAME_SESSION, session);

        database.insert(
                SubjectsContact.Subjects.TABLE_NAME,
                null,
                values);
    }


    public String makePostRequest() throws IOException {
        URL u = new URL(InfoExecutor.URL_STUD_PROGRESS);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.addRequestProperty("Cookie", paramsLogin.get("Cookie"));
        conn.addRequestProperty("Connection", "keep-alive");

        paramsLogin.remove("Cookie");

        String query = LoginExecutor.getQuery(paramsLogin); //Create query string, will be logged
        conn.setInstanceFollowRedirects(true);

        OutputStream os = conn.getOutputStream();
        os.write(query.getBytes("UTF-8"));
        os.close();
        conn.connect();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String inputLine;
        StringBuffer sb = new StringBuffer();
        while ((inputLine = in.readLine()) != null)
            sb.append(inputLine).append("\n");
        in.close();

        return sb.toString();
    }

    public void prepareParams() throws IOException {

        URL u = new URL(InfoExecutor.URL_STUD_PROGRESS);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();

        conn.setRequestMethod("GET");
        conn.addRequestProperty("Cookie", paramsLogin.get("Cookie"));

        BufferedReader in = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String inputLine;
        StringBuffer sb = new StringBuffer();
        while ((inputLine = in.readLine()) != null)
            sb.append(inputLine).append("\n");
        in.close();

        Document doc = Jsoup.parse(sb.toString());

        String ev = doc.getElementById(CookieReturner.EVENTVALIDATION_KEY).val();
        String viewstate = doc.getElementById(CookieReturner.VIEWSTATE_KEY).val();
        String viewstategenerator = doc.getElementById(CookieReturner.VIEWSTATEGENERATOR_KEY).val();
        String someParam = doc.getElementById(SOME_PARAMETER_ID).val();

        paramsLogin.put(CookieReturner.EVENTTARGET_KEY, "");
        paramsLogin.put(CookieReturner.EVENTARGUMENT_KEY, "");
        paramsLogin.put(CookieReturner.VIEWSTATE_KEY, viewstate);
        paramsLogin.put(CookieReturner.VIEWSTATEGENERATOR_KEY, viewstategenerator);
        paramsLogin.put(CookieReturner.EVENTVALIDATION_KEY, ev);

        paramsLogin.put(SOME_PARAMETER_FOR_POST, someParam);
        paramsLogin.put(SEMESTER_ID, "0");
    }
}
