package com.bsucabinet.ace.tasks;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import javax.security.auth.login.LoginException;

/**
 * Created by ace on 12.08.16.
 */
public class LoginSaveInfo extends AsyncTask <String, Void, Void> {

    private SharedPreferences sharedPreferences;

    public LoginSaveInfo(SharedPreferences sp) {
        super();
        sharedPreferences = sp;
    }

    @Override
    protected Void doInBackground(String... strings) {

        String surname = strings[0];
        String studNum = strings[1];

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Surname", surname);
        editor.putString("StudNum", studNum);
        editor.commit();

        return null;
    }
}
