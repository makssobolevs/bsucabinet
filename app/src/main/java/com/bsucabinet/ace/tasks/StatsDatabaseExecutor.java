package com.bsucabinet.ace.tasks;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.bsucabinet.ace.database.SubjectsContact;
import com.bsucabinet.ace.interfaces.AsyncResponse;

import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;


public class StatsDatabaseExecutor extends AsyncTask<Void, Void, Void> {

    SQLiteDatabase database;
    AsyncResponse listener;
    DecimalFormat formatter;
    SharedPreferences sharedPreferences;



    private String averMark;

    private ArrayList<AbstractMap.SimpleEntry<String, String>> worstList;
    private ArrayList<AbstractMap.SimpleEntry<String, String>> bestList;

    private AbstractMap.SimpleEntry<String, String> lastSession;
    private HashMap<String, Double> sessionMarks;
    private HashMap<Integer, Integer> histoData;

    public StatsDatabaseExecutor(SQLiteDatabase db, SharedPreferences sp) {
        super();
        database = db;
        formatter = new DecimalFormat("#.##");
        listener = null;
        sharedPreferences = sp;
    }

    @Override
    protected Void doInBackground(Void ... params) {

        getAverageMarkFromDB();
        getBestWorstSessionFromDB();
        if (sharedPreferences.contains("Course")) {
            String course = sharedPreferences.getString("Course", null);
            getLastSessionFromDB(course);
        }
        getHistoDataFromDB();

        return null;
    }

    private void getLastSessionFromDB(String course) {
        Cursor cursor = database.rawQuery("SELECT avg(cast(mark as float)) as aver, session " +
                "FROM subjects  GROUP BY session" +
                " HAVING session LIKE '%"+ course + "%' ORDER BY session", null);

        if (cursor.getCount() == 0) {
            try {
                int newCourse = Integer.parseInt(course) - 1;
                getLastSessionFromDB(Integer.toString(newCourse));
            }
            catch (NumberFormatException e) {  }
        }
        else {
            if (cursor.moveToFirst()) {
                double aver = cursor.getDouble(cursor.getColumnIndex("aver"));
                String session = cursor.getString(cursor.getColumnIndex("session"));
                lastSession = new AbstractMap.SimpleEntry<>(session, formatter.format(aver));
            }
        }
        cursor.close();

    }

    public void getHistoDataFromDB() {
        Cursor cursor = database.rawQuery("SELECT count(*), mark FROM subjects GROUP BY mark", null);

        histoData = new HashMap<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                int mark = cursor.getInt(cursor.getColumnIndex(
                        SubjectsContact.Subjects.COLUMN_NAME_MARK
                ));
                int count = cursor.getInt(0);
                histoData.put(mark, count);
                cursor.moveToNext();
            }

        }
        cursor.close();
    }

    private void getAverageMarkFromDB() {
        Cursor cursor = database.rawQuery("SELECT AVG(CAST(mark AS FLOAT)) AS aver FROM subjects", null);
        cursor.moveToFirst();
        double averMarkDouble = cursor.getDouble(0);
        averMark = formatter.format(averMarkDouble);
        cursor.close();
    }


    private void getBestWorstSessionFromDB() {
        Cursor cursor = database.rawQuery("SELECT avg(cast(mark as float)) as aver, session " +
                "FROM subjects GROUP BY session ORDER BY aver", null);
        sessionMarks = new HashMap<>();

        worstList = new ArrayList<>();
        bestList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            String worstSessionName = cursor.getString(cursor.getColumnIndex("session"));
            double worstSessionMark = cursor.getDouble(cursor.getColumnIndex("aver"));
            String wSMS = formatter.format(worstSessionMark);
            worstList.add(new AbstractMap.SimpleEntry<>(worstSessionName, wSMS));
            if (!cursor.isAfterLast()) {
                cursor.moveToNext();
                //In not one worst session
                while (!cursor.isAfterLast() &&
                        cursor.getDouble(cursor.getColumnIndex("aver")) == worstSessionMark) {
                    worstList.add(new AbstractMap.SimpleEntry<>(
                            cursor.getString(cursor.getColumnIndex("session")),
                            formatter.format(
                                cursor.getDouble(cursor.getColumnIndex("aver"))
                            )
                    ));
                    cursor.moveToNext();
                }
            }
        }

        if (cursor.moveToLast()) {
            String bestSessionName = cursor.getString(cursor.getColumnIndex("session"));
            double bestSessionMark = cursor.getFloat(cursor.getColumnIndex("aver"));
            String bSMS = formatter.format(bestSessionMark);
            bestList.add(new AbstractMap.SimpleEntry<>(bestSessionName, bSMS));
            if (!cursor.isBeforeFirst()) {
                cursor.moveToPrevious();
                //In not one best session
                while (!cursor.isBeforeFirst() &&
                        cursor.getDouble(cursor.getColumnIndex("aver")) == bestSessionMark) {
                    bestList.add(new AbstractMap.SimpleEntry<>(
                            cursor.getString(cursor.getColumnIndex("session")),
                            formatter.format(
                                cursor.getDouble(cursor.getColumnIndex("aver"))
                            )
                    ));
                    cursor.moveToPrevious();
                }
            }
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            double mark = cursor.getFloat(cursor.getColumnIndex("aver"));
            String session = cursor.getString(cursor.getColumnIndex("session"));
            sessionMarks.put(session, mark);
            cursor.moveToNext();
        }
        cursor.close();
    }

    public void setListener(AsyncResponse listener) {
        if (listener != null) {
            this.listener = listener;
        }
    }

    public String getAverMark() {
        return averMark;
    }

    public HashMap<String, Double> getSessionMarks() {
        return sessionMarks;
    }

    public HashMap<Integer, Integer> getHistoData() {
        return histoData;
    }

    @Override
    protected void onPostExecute(Void result) {
        listener.onTaskFinish();
    }

    public ArrayList<AbstractMap.SimpleEntry<String, String>> getWorstList() {
        return worstList;
    }

    public ArrayList<AbstractMap.SimpleEntry<String, String>> getBestList() {
        return bestList;
    }

    public AbstractMap.SimpleEntry<String, String> getLastSession() {
        return lastSession;
    }
}
