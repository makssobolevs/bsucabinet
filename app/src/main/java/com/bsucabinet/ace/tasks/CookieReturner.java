package com.bsucabinet.ace.tasks;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.os.AsyncTask;

import com.bsucabinet.ace.interfaces.AsyncResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CookieReturner extends AsyncTask<String, Void, Map<String, String>> {

    static final String COOKIE_KEY = "Cookie";
    static final String VIEWSTATE_KEY = "__VIEWSTATE";
    static final String VIEWSTATEGENERATOR_KEY = "__VIEWSTATEGENERATOR";
    static final String EVENTVALIDATION_KEY = "__EVENTVALIDATION";
    static final String EVENTTARGET_KEY = "__EVENTTARGET";
    static final String EVENTARGUMENT_KEY = "__EVENTARGUMENT";

    private AsyncResponse listener;

    public CookieReturner() {
        super();
    }

    @Override
    protected Map<String, String> doInBackground(String ... urls) {
        try {
            String url = urls[0];
            HttpURLConnection conn =(HttpURLConnection) new URL(url).openConnection();


            conn.connect();
            String[] cookies = conn.getHeaderFields().get("Set-Cookie").get(0).split("; ");
            String cookie = cookies[0];

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String inputLine;
            StringBuffer sb = new StringBuffer();
            while ((inputLine = in.readLine()) != null)
                sb.append(inputLine).append("\n");
            in.close();

            Document doc = Jsoup.parse(sb.toString());
            String viewstate = doc.getElementById(VIEWSTATE_KEY).val();
            String viewstategenerator = doc.getElementById(VIEWSTATEGENERATOR_KEY).val();
            String eventvalidation = doc.getElementById(EVENTVALIDATION_KEY).val();

            Map<String, String> map = new HashMap<>();
            map.put(COOKIE_KEY, cookie);
            map.put(EVENTTARGET_KEY, "");
            map.put(EVENTARGUMENT_KEY, "");
            map.put(VIEWSTATE_KEY, viewstate);
            map.put(VIEWSTATEGENERATOR_KEY, viewstategenerator);
            map.put(EVENTVALIDATION_KEY, eventvalidation);

            return map;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute(Map<String, String> result) {
        if (listener != null) {
            listener.onTaskFinish();
        }
    }

    public void setListener(AsyncResponse listener) {
        this.listener = listener;
    }

}
