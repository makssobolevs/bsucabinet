package com.bsucabinet.ace.tasks;


import android.os.AsyncTask;


import com.bsucabinet.ace.activities.LoginActivity;
import com.bsucabinet.ace.interfaces.AsyncResponse;
import com.bsucabinet.ace.interfaces.ErrorLoginResponse;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import javax.security.auth.login.LoginException;


public class LoginExecutor extends AsyncTask<Void, Void, String> {

    private Map<String, String> paramsLogin;
    private AsyncResponse listener;

    private ErrorLoginResponse errorListener;
    private Exception exception;



    @Override
    protected void onPostExecute(String string) {
        if (exception == null) {
            if (listener != null) {
                listener.onTaskFinish();
            }
        }
        else {
            if (errorListener != null) {
                errorListener.onErrorLogin();
            }
        }
    }

    @Override
    protected String doInBackground(Void ... params) {

        String auth_cookie = null;

        try {
            URL u = new URL(LoginActivity.URL);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            conn.addRequestProperty("Cookie", paramsLogin.get("Cookie"));
            conn.addRequestProperty("Connection", "keep-alive");

            paramsLogin.remove(0);
            String toPush = getQuery(paramsLogin);

            conn.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setInstanceFollowRedirects(false);

            OutputStream os = conn.getOutputStream();
            os.write(toPush.getBytes("UTF-8"));
            os.close();

            conn.connect();

            if (conn.getResponseCode() != 302) {
                throw new LoginException();
            }

            System.out.println("Login response code = " + conn.getResponseCode());
            auth_cookie = conn.getHeaderField("Set-Cookie").split(";")[0];
        }
        catch (LoginException e) {
            exception = e;
        }
        catch (MalformedURLException | ProtocolException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return auth_cookie;
    }

    public static String getQuery(Map<String, String> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> pair : params.entrySet())
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        System.out.println(result.toString());
        return result.toString();
    }

    public void setListener(AsyncResponse listener) {
        this.listener = listener;
    }

    public void setParamsLogin(Map<String, String> paramsLogin) {
        this.paramsLogin = paramsLogin;
    }

    public void setErrorListener(ErrorLoginResponse errorListener) {
        this.errorListener = errorListener;
    }

}
