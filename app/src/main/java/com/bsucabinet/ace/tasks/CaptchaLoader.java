package com.bsucabinet.ace.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.bsucabinet.ace.activities.LoginActivity;
import com.bsucabinet.ace.interfaces.AsyncResponse;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class CaptchaLoader extends AsyncTask<String, Void, Bitmap> {

    private static final String TAG = "CaptchaLoader";

    private AsyncResponse listener;

    public CaptchaLoader() {
        super();
    }

    @Override
    protected Bitmap doInBackground(String ... params) {
        String captcha_url = LoginActivity.CAPTCHA_URL;
        String cookie = params[0];
        try {

            HttpURLConnection connCaptcha =(HttpURLConnection) new URL(captcha_url).openConnection();
            connCaptcha.setRequestMethod("GET");
            connCaptcha.addRequestProperty("Cookie", cookie);

            connCaptcha.connect();

            InputStream in = connCaptcha.getInputStream();
            Bitmap captcha = BitmapFactory.decodeStream(in);
            return captcha;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (listener != null) {
            listener.onTaskFinish();
        }
    }

    public void setListener(AsyncResponse listener) {
        this.listener = listener;
    }


}
