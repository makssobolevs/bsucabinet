package com.bsucabinet.ace.tasks;


import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.bsucabinet.ace.interfaces.AsyncResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class InfoExecutor extends AsyncTask <Void, Void, ArrayList<String>>
{

    private static final String URL = "http://student.bsu.by/ChooseAction.aspx";
    static final String URL_STUD_PROGRESS = "http://student.bsu.by/StudProgress.aspx";
    private static final String EVENTTARGET_STUD_PROGRESS = "ChooseActionMenu1$StudProgress";
    private static final String USERNAME_ID = "ctlStudProgress1_lbStudName";
    private static final String FACULTY_ID = "ctlStudProgress1_lbStudFacultet";
    private static final String DATA_ID = "ctlStudProgress1_lbStudKurs";
    Map<String, String> paramsLogin;

    private AsyncResponse delegate;
    private SharedPreferences sharedPreferences;

    public InfoExecutor(AsyncResponse delegate, Map<String, String> paramsLogin, SharedPreferences sp) {
        super();
        this.delegate = delegate;
        this.paramsLogin = paramsLogin;
        sharedPreferences = sp;
    }

    @Override
    public ArrayList<String> doInBackground(Void ... params) {
        try {
            URL u = new URL(URL_STUD_PROGRESS);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();

            conn.setRequestMethod("GET");
            conn.addRequestProperty("Cookie", paramsLogin.get("Cookie"));

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String inputLine;
            StringBuffer sb = new StringBuffer();
            while ((inputLine = in.readLine()) != null)
                sb.append(inputLine).append("\n");
            in.close();

            Document doc = Jsoup.parse(sb.toString());

            String username = doc.getElementById(USERNAME_ID).text();
            String faculty = doc.getElementById(FACULTY_ID).text();
            String[] data = doc.getElementById(DATA_ID).text().split(", ");

            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (data[0].contains("курс")) {
                editor.putString("Course", data[0].replaceAll("[^0-9]", ""));
            }
            editor.commit();


            ArrayList<String> list = new ArrayList<>();
            list.add(username);
            list.add(faculty);
            Collections.addAll(list, data);

            return list;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onPostExecute(ArrayList<String> result) {
        delegate.onTaskFinish();
    }
}
