package com.bsucabinet.ace.tasks;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ace on 10.08.16.
 */
public class SaveInfo extends AsyncTask<Void, Void, Void> {

    ArrayList<String> info;
    SharedPreferences sharedPreferences;

    public SaveInfo(ArrayList<String> info, SharedPreferences sp) {
        super();
        this.info = info;
        sharedPreferences = sp;
    }

    @Override
    protected Void doInBackground(Void ... params) {
        JSONArray jsonArray = new JSONArray(info);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Info", jsonArray.toString());
        editor.commit();
        return null;
    }
}
