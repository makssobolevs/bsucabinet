package com.bsucabinet.ace.tasks;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public abstract class NetworkChecker {
    public static boolean isConntected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
}
