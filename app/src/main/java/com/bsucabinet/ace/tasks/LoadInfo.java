package com.bsucabinet.ace.tasks;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.bsucabinet.ace.interfaces.AsyncResponse;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class LoadInfo extends AsyncTask<Void, Void, ArrayList<String>> {

    SharedPreferences sharedPreferences;
    AsyncResponse delegate;

    public LoadInfo(SharedPreferences sp, AsyncResponse activity) {
        super();
        sharedPreferences = sp;
        delegate = activity;
    }


    @Override
    protected ArrayList<String> doInBackground(Void ... params) {
        String jstring = sharedPreferences.getString("Info", null);

        ArrayList<String> info = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(jstring);

            for (int i = 0; i < jsonArray.length(); i++) {
                info.add((String)jsonArray.get(i));
            }

//            Iterator x = jsonObject.keys();
//            while (x.hasNext()) {
//                info.add((String)x.next());
//            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return info;
    }

    @Override
    protected void onPostExecute(ArrayList<String> info) {
        delegate.onTaskFinish();
    }
}
