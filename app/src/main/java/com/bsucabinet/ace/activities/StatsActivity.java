package com.bsucabinet.ace.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import com.bsucabinet.ace.database.SubjectDbHelper;
import com.bsucabinet.ace.database.SubjectsContact;
import com.bsucabinet.ace.interfaces.AsyncResponse;
import com.bsucabinet.ace.tasks.NetworkChecker;
import com.bsucabinet.ace.tasks.StatsDatabaseExecutor;
import com.bsucabinet.ace.tasks.StatsExecutor;
import com.bsucabinet.ace.ui_components.HistoBar;

public class StatsActivity extends BaseActivity implements AsyncResponse {

    private SubjectDbHelper mDbHelper;
    private HistoBar histoBar;
    private TextView textViewBestSession, textViewWorstSession, textViewAver, textViewLast;
    private View[] labels;
    private StatsDatabaseExecutor statsDatabaseExecutor;
    private StatsExecutor statsExecutor;
    private Toast toastUpdate;
    private boolean isFullUpdate;
    private boolean isButtonLocked;
    private boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        mDbHelper = new SubjectDbHelper(getApplicationContext());

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_stats);
        setSupportActionBar(myToolbar);

        histoBar = (HistoBar) findViewById(R.id.HistoBar);
        textViewBestSession = (TextView) findViewById(R.id.textBestSession);
        textViewWorstSession = (TextView) findViewById(R.id.textWorstSession);
        textViewAver = (TextView) findViewById(R.id.textAverage);
        textViewLast = (TextView) findViewById(R.id.textLast);


        labels = new View[] {
                (TextView) findViewById(R.id.labelBestSession),
                (TextView) findViewById(R.id.labelWorstSession),
                (TextView) findViewById(R.id.labelAverage),
                (TextView) findViewById(R.id.labelLast),
                (TextView) findViewById(R.id.labelHistoCaption),
                textViewBestSession, textViewLast, textViewWorstSession,
                textViewAver, histoBar
        };

        setTitle(getResources().getString(R.string.stats_title));
        toastUpdate = null;
        isFullUpdate = false;
        isButtonLocked = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        isConnected = NetworkChecker.isConntected(getApplicationContext());
        try {
            if (isTableEmpty()) {
                if (isConnected) {
                    loadSubjectData();
                }
                else {
                    showToastNoNetwork();
                }

            } else {
                onTaskFinish();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    public void showToastNoNetwork() {
        Context context = getApplicationContext();
        String text = getResources().getString(R.string.toast_no_network);
        int duration = Toast.LENGTH_SHORT;
        Toast toastConnection = Toast.makeText(context, text, duration);
        toastConnection.show();
    }


    public void onTaskFinish() {
        //showDataTest();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        SharedPreferences sp = getSharedPreferences(InfoScreenActivity.PREFERENCES, MODE_PRIVATE);
        statsDatabaseExecutor = new StatsDatabaseExecutor(db, sp);
        statsDatabaseExecutor.setListener(new AsyncResponse() {
            @Override
            public void onTaskFinish() {
                String averMark = statsDatabaseExecutor.getAverMark();
                textViewAver.setText(averMark);

                ArrayList<AbstractMap.SimpleEntry<String, String>> bestSession =
                        statsDatabaseExecutor.getBestList();
                ArrayList<AbstractMap.SimpleEntry<String, String>> worstSession =
                        statsDatabaseExecutor.getWorstList();


                showBestWorstSessionField(textViewBestSession, bestSession);
                showBestWorstSessionField(textViewWorstSession, worstSession);

                AbstractMap.SimpleEntry entry = statsDatabaseExecutor.getLastSession();

                textViewLast.setText(entry.getKey() + " c " + entry.getValue());

                HashMap<Integer, Integer> marks = statsDatabaseExecutor.getHistoData();
                histoBar.setMarks(marks);
                for (int i = 0; i < labels.length; i++) {
                    labels[i].setVisibility(View.VISIBLE);
                }
                if (isFullUpdate) {
                    showUpdateToast();
                }
                isButtonLocked = false;
                if (isMenuLoaded) {
                    invalidateOptionsMenu();
                }

            }
        });
        statsDatabaseExecutor.execute();
    }

    public void showBestWorstSessionField(TextView tw,
                                          ArrayList<AbstractMap.SimpleEntry<String, String>> list) {
        if (list.size() > 0) {
            tw.setText("");
            int i = 0;
            for (i = 0; i < list.size() - 1; i++) {
                String key = list.get(i).getKey();
                String value = list.get(i).getValue();
                tw.append(key + " c " + value);
                tw.append("\n");
            }
            String key = list.get(i).getKey();
            String value = list.get(i).getValue();
            tw.append(key + " c " + value);
        }
    }

    public void showUpdateToast() {
        if (toastUpdate == null) {
            Context context = getApplicationContext();
            String text = getResources().getString(R.string.toast_stats_update);
            int duration = Toast.LENGTH_SHORT;
            toastUpdate = Toast.makeText(context, text, duration);
        }
        toastUpdate.show();
    }

    public boolean isTableEmpty() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        int n = 0;
        try {
            Cursor cursor = db.rawQuery("SELECT count(*) FROM subjects", null);
            cursor.moveToFirst();
            n = cursor.getInt(0);
            if (n == 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void loadSubjectData() throws ExecutionException, InterruptedException {
        isFullUpdate = true;
        isButtonLocked = true;
        if (isMenuLoaded) {
            refreshMenuItem.setActionView(R.layout.layout_progress_bar);
        }
        HashMap<String, String> paramsLogin = (HashMap<String, String>)
                getIntent().getSerializableExtra("map");
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        System.out.println("Start loading data");
        statsExecutor = new StatsExecutor(this, db, paramsLogin);
        statsExecutor.execute();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if ((statsExecutor != null && statsExecutor.getStatus() == AsyncTask.Status.RUNNING)
            || (statsDatabaseExecutor != null &&
                statsDatabaseExecutor.getStatus() == AsyncTask.Status.RUNNING)) {
            refreshMenuItem.setActionView(R.layout.layout_progress_bar);
        }
        return true;
    }

    public boolean refreshAction(MenuItem item) {
        isConnected = NetworkChecker.isConntected(getApplicationContext());
        if (isConnected) {
            try {
                if (!isButtonLocked) {
                    loadSubjectData();
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        else {
            showToastNoNetwork();
        }
        return true;
    }

    public void watchTables() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master", null);
        cursor.moveToFirst();
        //System.out.println(cursor.getCount());
        while (!cursor.isAfterLast()) {
            System.out.println(cursor.getString(cursor.getColumnIndex("name")));
            cursor.moveToNext();
        }
    }


    public void showDataTest() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                SubjectsContact.Subjects.COLUMN_NAME_SUBJECT_TITLE,
                SubjectsContact.Subjects.COLUMN_NAME_MARK,
                SubjectsContact.Subjects.COLUMN_NAME_SESSION
        };

        String sortOrder =
                SubjectsContact.Subjects.COLUMN_NAME_SESSION + " DESC";

        Cursor cursor = db.rawQuery("SELECT * FROM subjects", null);

        StringBuffer sb = new StringBuffer();

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String subj = cursor.getString(cursor.getColumnIndex(
                        SubjectsContact.Subjects.COLUMN_NAME_SUBJECT_TITLE
                ));
                String session = cursor.getString(cursor.getColumnIndex(
                        SubjectsContact.Subjects.COLUMN_NAME_SESSION
                ));
                int mark = cursor.getInt(cursor.getColumnIndex(
                        SubjectsContact.Subjects.COLUMN_NAME_MARK
                ));
                sb.append(subj).append(" ").append(session).append(" ");
                sb.append(Integer.toString(mark)).append("\n");
                cursor.moveToNext();
            }
        }
        System.out.println(sb.toString());
    }
}
