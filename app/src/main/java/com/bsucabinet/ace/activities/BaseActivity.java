package com.bsucabinet.ace.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public abstract class BaseActivity  extends AppCompatActivity{

    protected boolean isMenuLoaded = false;
    protected MenuItem refreshMenuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        System.out.println("Menu created");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        refreshMenuItem = menu.findItem(R.id.action_refresh);
        isMenuLoaded = true;
        return true;
    }

    public abstract boolean refreshAction(MenuItem item);

    public boolean infoAction(MenuItem item)
    {
        startActivity(new Intent(this, InfoPopup.class));
        return true;
    }
}
