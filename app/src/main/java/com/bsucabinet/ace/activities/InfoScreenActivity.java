package com.bsucabinet.ace.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import com.bsucabinet.ace.interfaces.AsyncResponse;
import com.bsucabinet.ace.tasks.InfoExecutor;
import com.bsucabinet.ace.tasks.LoadInfo;
import com.bsucabinet.ace.tasks.NetworkChecker;
import com.bsucabinet.ace.tasks.SaveInfo;

public class InfoScreenActivity extends BaseActivity implements AsyncResponse {

    private HashMap<String, String> paramsLogin;
    private ArrayList<String> infoList;
    private InfoExecutor infoExecutor;
    private LoadInfo loadInfoExecutor;
    private Toast refreshToast = null;
    private boolean isInfoFromPrefs = false;
    private ListView listView;
    private boolean isConnected;


    private SharedPreferences sharedpreferences;
    public static final String PREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_screen);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_info);
        setSupportActionBar(myToolbar);
        //getSupportActionBar().setIcon(R.drawable.ic_icon_launcher);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        listView = (ListView) findViewById(R.id.listView);


        sharedpreferences = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);

        isConnected = NetworkChecker.isConntected(getApplicationContext());

        if (sharedpreferences.contains("Cookie")) {

            paramsLogin = new HashMap<>();
            paramsLogin.put("Cookie", sharedpreferences.getString("Cookie", null));

            if (sharedpreferences.contains("Info")) {
                isInfoFromPrefs = true;
                loadInfoExecutor = new LoadInfo(sharedpreferences, this);
                loadInfoExecutor.execute();
            }
            else {
                if (isConnected) {
                    loadInfo();
                }
                else {
                    showToastNoNetwork();
                }
            }

        }
        else {
            startLoginActivity();
            finish();
        }

        letterToMe();

    }

    public void letterToMe() {
        TextView tw = (TextView) findViewById(R.id.letterToMe);
        tw.setText(Html.fromHtml("<a href=\"mailto:bsucab@gmail.com\">Если в приложении" +
                " тебе не хватает инфы об оплате " +
                "за общагу, пришли по почте номер своего студенческого и фамилию. Запилим</a>"));
        tw.setMovementMethod(LinkMovementMethod.getInstance());
    }


    public void onTaskFinish() {
        try {
            if (isInfoFromPrefs) {
                infoList = loadInfoExecutor.get();
            }
            else {
                infoList = infoExecutor.get();
            }

            if (infoList == null || infoList.size() == 0 || infoList.get(0).isEmpty()) {
                startLoginActivity();
                finish();
            }
            else {
                ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                        R.layout.data_listview, infoList);
                listView.setAdapter(adapter);

                new SaveInfo(infoList, sharedpreferences).execute();

                if (refreshToast == null) {
                    Context context = getApplicationContext();
                    String text = getResources().getString(R.string.toast_info_update);
                    int duration = Toast.LENGTH_SHORT;
                    refreshToast = Toast.makeText(context, text, duration);
                }
                if (!isInfoFromPrefs) {
                    refreshToast.show();
                }
                if (isMenuLoaded) {
                    invalidateOptionsMenu();
                }
            }
        }
        catch (ExecutionException |InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void loadInfo() {
        isInfoFromPrefs = false;
        infoExecutor = new InfoExecutor(this, paramsLogin, sharedpreferences);
        infoExecutor.execute();
    }

    public void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    public void onClickStats(View view) {
        Intent intent = new Intent(this, StatsActivity.class);
        intent.putExtra("map", paramsLogin);
        startActivity(intent);
    }


    public boolean refreshAction(MenuItem item) {
        isConnected = NetworkChecker.isConntected(getApplicationContext());
        if (isConnected) {
            refreshMenuItem.setActionView(R.layout.layout_progress_bar);
            loadInfo();
        }
        else {
            showToastNoNetwork();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    public void showToastNoNetwork() {
        Context context = getApplicationContext();
        String text = getResources().getString(R.string.toast_no_network);
        int duration = Toast.LENGTH_SHORT;
        Toast toastConnection = Toast.makeText(context, text, duration);
        toastConnection.show();
    }

}
