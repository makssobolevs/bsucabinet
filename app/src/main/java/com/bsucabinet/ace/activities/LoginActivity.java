package com.bsucabinet.ace.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;
import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.bsucabinet.ace.interfaces.AsyncResponse;
import com.bsucabinet.ace.interfaces.ErrorLoginResponse;
import com.bsucabinet.ace.tasks.CaptchaLoader;
import com.bsucabinet.ace.tasks.CookieReturner;
import com.bsucabinet.ace.tasks.LoginExecutor;
import com.bsucabinet.ace.tasks.LoginSaveInfo;


public class LoginActivity extends AppCompatActivity
        implements ErrorLoginResponse {

    private EditText textSurname, textNumber, textCaptcha;
    private TextView errorView;
    private ImageView imageView;
    private CaptchaLoader captchaLoader;
    private CookieReturner cookieReturner;
    private LoginExecutor loginExecutor;

    public static final String URL = "http://student.bsu.by/Login.aspx";
    public static final String CAPTCHA_URL = "http://student.bsu.by/CaptchaImage.aspx";


    private static final String TAG = "LoginWindow";
    private AppCompatActivity thisActivity = this;

    private Map<String, String> paramsLogin;

    private static final String HTML_INPUT_SURNAME = "tbFam";
    private static final String HTML_INPUT_NUMBER = "tbNumStud";
    private static final String HTML_INPUT_CAPTCHA = "TextBox1";
    private static final String HTML_INPUT_DOGOVOR = "tbNumDogovor";
    private static final String HTML_SUBMIT_BUTTON = "Button1";
    private static final String BUTTON_VALUE = "Вход";

    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences(InfoScreenActivity.PREFERENCES,
                Context.MODE_PRIVATE);

        textSurname = (EditText) findViewById(R.id.editTextSurname);
        textNumber = (EditText) findViewById(R.id.editTextNumber);
        textCaptcha = (EditText) findViewById(R.id.editTextCaptcha);
        imageView = (ImageView) findViewById(R.id.imageView);
        errorView = (TextView) findViewById(R.id.textErrorLogin);

        if (sharedpreferences.contains("Surname")) {
            textSurname.setText(sharedpreferences.getString("Surname", ""));
            textNumber.setText(sharedpreferences.getString("StudNum", ""));
        }

        initListeners();

        cookieReturner.execute(URL);
    }

    public void initListeners() {
        cookieReturner = new CookieReturner();
        cookieReturner.setListener(new AsyncResponse() {
            @Override
            public void onTaskFinish() {
                try {
                    paramsLogin = cookieReturner.get();
                    captchaLoader.execute(paramsLogin.get("Cookie"));
                }
                catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        captchaLoader = new CaptchaLoader();
        captchaLoader.setListener(new AsyncResponse() {
            @Override
            public void onTaskFinish() {
                try {
                    Bitmap captcha = captchaLoader.get();
                    imageView.setImageBitmap(Bitmap.createScaledBitmap(captcha, 240, 60, false));
                }
                catch (InterruptedException | ExecutionException e) {

                }
            }
        });

        loginExecutor = new LoginExecutor();
        loginExecutor.setErrorListener(this);
        loginExecutor.setListener(new AsyncResponse() {
            @Override
            public void onTaskFinish() {
                try {
                    String auth_cookie = loginExecutor.get();
                    String loggedCookie = paramsLogin.get("Cookie") + "; " + auth_cookie;

                    paramsLogin.put("Cookie", loggedCookie);

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("Cookie", loggedCookie);
                    editor.commit();

                    Intent intent = new Intent(thisActivity, InfoScreenActivity.class);
                    intent.putExtra("map", (HashMap<String, String>)paramsLogin);
                    startActivity(intent);
                    new LoginSaveInfo(sharedpreferences).execute(
                            textSurname.getText().toString(),
                            textNumber.getText().toString()
                    );
                    finish();
                }
                catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void onClick(View view) {
        setParameters();
        loginExecutor.setParamsLogin(paramsLogin);
        loginExecutor.execute();
    }

    public void onErrorLogin() {
        errorView.setVisibility(View.VISIBLE);
        initListeners();
        cookieReturner.execute(URL);
        textCaptcha.setText("");
    }


    public void setParameters() {
        String surname = textSurname.getText().toString();
        String numStud = textNumber.getText().toString();
        String captchaValue = textCaptcha.getText().toString();

        paramsLogin.put(HTML_INPUT_SURNAME, surname);
        paramsLogin.put(HTML_INPUT_DOGOVOR, "");
        paramsLogin.put(HTML_INPUT_NUMBER, numStud);
        paramsLogin.put(HTML_INPUT_CAPTCHA, captchaValue);
        paramsLogin.put(HTML_SUBMIT_BUTTON, BUTTON_VALUE);
    }

}
