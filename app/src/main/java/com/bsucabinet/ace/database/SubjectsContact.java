package com.bsucabinet.ace.database;


import android.provider.BaseColumns;

public final class SubjectsContact {
    public SubjectsContact() { }

    public static abstract class Subjects implements BaseColumns {
        public static final String TABLE_NAME = "subjects";
        public static final String COLUMN_NAME_SUBJECT_TITLE = "subject_title";
        public static final String COLUMN_NAME_MARK = "mark";
        public static final String COLUMN_NAME_SESSION = "session";

        private static final String TEXT_TYPE = " TEXT";
        private static final String COMMA_SEP = ",";
        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + Subjects.TABLE_NAME + " (" +
                        Subjects._ID + " INTEGER PRIMARY KEY," +
                        Subjects.COLUMN_NAME_SUBJECT_TITLE + TEXT_TYPE + COMMA_SEP +
                        Subjects.COLUMN_NAME_MARK + " INTEGER" + COMMA_SEP +
                        Subjects.COLUMN_NAME_SESSION + TEXT_TYPE + " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + Subjects.TABLE_NAME;
    }


}
