package com.bsucabinet.ace.interfaces;

public interface ErrorLoginResponse {

    public void onErrorLogin();
}
