package com.bsucabinet.ace.interfaces;

/**
 * Created by ace on 09.08.16.
 */
public interface AsyncResponse {
    void onTaskFinish();
}
